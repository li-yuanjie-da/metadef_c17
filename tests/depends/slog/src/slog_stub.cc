/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "toolchain/slog.h"
#include "toolchain/plog.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <map>

void dav_log(int module_id, const char *fmt, ...) {}

static int log_level = DLOG_ERROR;

static std::map<int, std::string> log_level_map {{DLOG_ERROR, "[ERROR]"},
                                                 {DLOG_WARN, "[WARN]"},
                                                 {DLOG_INFO, "[INFO]"},
                                                 {DLOG_DEBUG, "[DEBUG]"}};

#define __DO_PRINT(LEVEL)                            \
  do {                                               \
    const int FMT_BUFF_SIZE = 1024;                  \
    char fmt_buff[FMT_BUFF_SIZE] = {0};              \
    va_list valist;                                  \
    va_start(valist, fmt);                           \
    vsnprintf(fmt_buff, FMT_BUFF_SIZE, fmt, valist); \
    va_end(valist);                                  \
    printf("%s %s \n",log_level_map[LEVEL].c_str(), fmt_buff);                  \
  } while (false)

void DlogRecord(int moduleId, int level, const char *fmt, ...) {
  if (log_level > level) {
    return;
  }
  __DO_PRINT(level);
}

void DlogErrorInner(int module_id, const char *fmt, ...) {
  if (log_level > DLOG_ERROR) {
    return;
  }
  __DO_PRINT(DLOG_ERROR);
}

void DlogWarnInner(int module_id, const char *fmt, ...) {
  if (log_level > DLOG_WARN) {
    return;
  }
  __DO_PRINT(DLOG_WARN);
}

void DlogInfoInner(int module_id, const char *fmt, ...) {
  if (log_level > DLOG_INFO) {
    return;
  }
  __DO_PRINT(DLOG_INFO);
}

void DlogDebugInner(int module_id, const char *fmt, ...) {
  if (log_level > DLOG_DEBUG) {
    return;
  }
  __DO_PRINT(DLOG_DEBUG);
}

void DlogEventInner(int module_id, const char *fmt, ...) { dav_log(module_id, fmt); }

void DlogInner(int module_id, int level, const char *fmt, ...) { dav_log(module_id, fmt); }

int dlog_setlevel(int module_id, int level, int enable_event) {
  log_level = level;
  return 0;
}

int dlog_getlevel(int module_id, int *enable_event) { return log_level; }

int CheckLogLevel(int moduleId, int log_level_check) { return log_level_check >= log_level; }

/**
 * @ingroup plog
 * @brief DlogReportInitialize: init log in service process before all device setting.
 * @return: 0: SUCCEED, others: FAILED
 */
int DlogReportInitialize() { return 0; }

/**
 * @ingroup plog
 * @brief DlogReportFinalize: release log resource in service process after all device reset.
 * @return: 0: SUCCEED, others: FAILED
 */
int DlogReportFinalize() { return 0; }
