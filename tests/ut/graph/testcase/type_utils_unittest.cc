/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph/utils/type_utils.h"
#include <climits>
#include <gtest/gtest.h>
#include "graph/debug/ge_util.h"

namespace ge {
class UtestTypeUtils : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() {}
};

TEST_F(UtestTypeUtils, FormatToSerialString) {
  ASSERT_EQ(TypeUtils::FormatToSerialString(FORMAT_NCHW), "NCHW");
  ASSERT_EQ(TypeUtils::FormatToSerialString(FORMAT_END), "END");
  ASSERT_EQ(TypeUtils::FormatToSerialString(static_cast<Format>(GetFormatFromSub(FORMAT_FRACTAL_Z, 1))), "FRACTAL_Z:1");
  ASSERT_EQ(TypeUtils::FormatToSerialString(static_cast<Format>(GetFormatFromSub(FORMAT_END, 1))), "END:1");
  ASSERT_EQ(TypeUtils::FormatToSerialString(FORMAT_C1HWC0), "C1HWC0");
}

TEST_F(UtestTypeUtils, SerialStringToFormat) {
  ASSERT_EQ(TypeUtils::SerialStringToFormat("NCHW"), FORMAT_NCHW);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("C1HWC0"), FORMAT_C1HWC0);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("INVALID"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:1"), GetFormatFromSub(FORMAT_FRACTAL_Z, 1));
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:01"), GetFormatFromSub(FORMAT_FRACTAL_Z, 1));
  ASSERT_EQ(TypeUtils::SerialStringToFormat("INVALID:1"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:1%"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:"), FORMAT_RESERVED);  // invalid_argument exception
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:65535"), GetFormatFromSub(FORMAT_FRACTAL_Z, 0xffff));
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:65536"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::SerialStringToFormat("FRACTAL_Z:9223372036854775807"), FORMAT_RESERVED);
}

TEST_F(UtestTypeUtils, DataFormatToFormat) {
  ASSERT_EQ(TypeUtils::DataFormatToFormat("NCHW"), FORMAT_NCHW);
  ASSERT_EQ(TypeUtils::DataFormatToFormat("INVALID"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::DataFormatToFormat("NCHW:1"), GetFormatFromSub(FORMAT_NCHW, 1));
  ASSERT_EQ(TypeUtils::DataFormatToFormat("NCHW:01"), GetFormatFromSub(FORMAT_NCHW, 1));
  ASSERT_EQ(TypeUtils::DataFormatToFormat("INVALID:1"), FORMAT_RESERVED);
  ASSERT_EQ(TypeUtils::DataFormatToFormat("NCHW:1%"), FORMAT_RESERVED);
}

TEST_F(UtestTypeUtils, DataTypeToSerialString) {
  ASSERT_EQ(TypeUtils::DataTypeToSerialString(DT_INT2), "DT_INT2");
  ASSERT_EQ(TypeUtils::DataTypeToSerialString(DT_UINT2), "DT_UINT2");
  ASSERT_EQ(TypeUtils::DataTypeToSerialString(DT_UINT1), "DT_UINT1");
  ASSERT_EQ(TypeUtils::DataTypeToSerialString(DT_MAX), "UNDEFINED");
}

TEST_F(UtestTypeUtils, SerialStringToDataType) {
  ASSERT_EQ(TypeUtils::SerialStringToDataType("DT_UINT1"), DT_UINT1);
  ASSERT_EQ(TypeUtils::SerialStringToDataType("DT_INT2"), DT_INT2);
  ASSERT_EQ(TypeUtils::SerialStringToDataType("DT_MAX"), DT_UNDEFINED);
}
}
