/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph/utils/type_utils_inner.h"
#include <climits>
#include <gtest/gtest.h>
#include "graph/debug/ge_util.h"

namespace ge {
class UtestTypeUtilsInner : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() {}
};

TEST_F(UtestTypeUtilsInner, IsDataTypeValid) {
  ASSERT_FALSE(TypeUtilsInner::IsDataTypeValid(DT_MAX));
  ASSERT_TRUE(TypeUtilsInner::IsDataTypeValid(DT_INT4));

  ASSERT_FALSE(TypeUtilsInner::IsDataTypeValid("MAX"));
  ASSERT_TRUE(TypeUtilsInner::IsDataTypeValid("UINT64"));
  ASSERT_TRUE(TypeUtilsInner::IsDataTypeValid("STRING_REF"));
}

TEST_F(UtestTypeUtilsInner, IsFormatValid) {
  ASSERT_TRUE(TypeUtilsInner::IsFormatValid(FORMAT_NCHW));
  ASSERT_FALSE(TypeUtilsInner::IsFormatValid(FORMAT_END));

  ASSERT_TRUE(TypeUtilsInner::IsFormatValid("DECONV_SP_STRIDE8_TRANS"));
  ASSERT_FALSE(TypeUtilsInner::IsFormatValid("FORMAT_END"));
}

TEST_F(UtestTypeUtilsInner, IsInternalFormat) {
  ASSERT_TRUE(TypeUtilsInner::IsInternalFormat(FORMAT_FRACTAL_Z));
  ASSERT_FALSE(TypeUtilsInner::IsInternalFormat(FORMAT_RESERVED));
}

TEST_F(UtestTypeUtilsInner, ImplyTypeToSSerialString) {
  ASSERT_EQ(TypeUtilsInner::ImplyTypeToSerialString(domi::ImplyType::BUILDIN), "buildin");
  ASSERT_EQ(TypeUtilsInner::ImplyTypeToSerialString(static_cast<domi::ImplyType>(30)), "UNDEFINED");
}

TEST_F(UtestTypeUtilsInner, DomiFormatToFormat) {
  ASSERT_EQ(TypeUtilsInner::DomiFormatToFormat(domi::domiTensorFormat_t::DOMI_TENSOR_NDHWC), FORMAT_NDHWC);
  ASSERT_EQ(TypeUtilsInner::DomiFormatToFormat(static_cast<domi::domiTensorFormat_t>(30)), FORMAT_RESERVED);
}

TEST_F(UtestTypeUtilsInner, FmkTypeToSerialString) {
  ASSERT_EQ(TypeUtilsInner::FmkTypeToSerialString(domi::FrameworkType::CAFFE), "caffe");
}

TEST_F(UtestTypeUtilsInner, CheckUint64MulOverflow) {
  ASSERT_FALSE(TypeUtilsInner::CheckUint64MulOverflow(0x00ULL, 0x00UL));
  ASSERT_FALSE(TypeUtilsInner::CheckUint64MulOverflow(0x02ULL, 0x01UL));
  ASSERT_TRUE(TypeUtilsInner::CheckUint64MulOverflow(0xFFFFFFFFFFFFULL, 0xFFFFFFFUL));
}

TEST_F(UtestTypeUtilsInner, ImplyTypeToSerialString) {
  ASSERT_EQ(TypeUtilsInner::ImplyTypeToSerialString(domi::ImplyType::BUILDIN), "buildin");
}

TEST_F(UtestTypeUtilsInner, DomiFormatToFormat2) {
  ASSERT_EQ(TypeUtilsInner::DomiFormatToFormat(domi::DOMI_TENSOR_NCHW), FORMAT_NCHW);
  ASSERT_EQ(TypeUtilsInner::DomiFormatToFormat(domi::DOMI_TENSOR_RESERVED), FORMAT_RESERVED);
}

TEST_F(UtestTypeUtilsInner, CheckUint64MulOverflow2) {
  ASSERT_FALSE(TypeUtilsInner::CheckUint64MulOverflow(0, 1));
  ASSERT_FALSE(TypeUtilsInner::CheckUint64MulOverflow(1, 1));
  ASSERT_TRUE(TypeUtilsInner::CheckUint64MulOverflow(ULLONG_MAX, 2));
}

TEST_F(UtestTypeUtilsInner, FmkTypeToSerialString2) {
  ASSERT_EQ(TypeUtilsInner::FmkTypeToSerialString(domi::CAFFE), "caffe");
  ASSERT_EQ(TypeUtilsInner::FmkTypeToSerialString(static_cast<domi::FrameworkType>(domi::FRAMEWORK_RESERVED + 1)), "");
}
}
