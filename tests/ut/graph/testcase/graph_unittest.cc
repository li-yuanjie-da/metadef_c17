/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <iostream>
#define private public
#include "graph/graph.h"
#include "graph/operator.h"
#include "graph/compute_graph.h"
#include "graph/compute_graph_impl.h"
#include "graph/op_desc.h"
#include "graph/node.h"
#include "graph/utils/graph_utils.h"
#include "external/graph/graph.h"
#include "graph/compute_graph_impl.h"
#include "inc/external/graph/operator_reg.h"
#include "inc/external/graph/operator.h"
#include "inc/external/graph/operator_factory.h"
#include "inc/external/graph/graph.h"
#include "inc/external/graph/graph_buffer.h"
#include "inc/graph/operator_factory_impl.h"
#include "graph/utils/op_desc_utils.h"
#include "graph/utils/graph_utils_ex.h"
#include "graph/utils/op_desc_utils_ex.h"
#include "graph_builder_utils.h"
#include "graph/ge_attr_value.h"
#include "ge_ir.pb.h"
#include "inc/common/ge_common/ge_inner_error_codes.h"
#undef private

using namespace ge;

class UtestGraph : public testing::Test {
 protected:
  void SetUp() {}

  void TearDown() {}
};

static ComputeGraphPtr BuildSubComputeGraph() {
  ut::GraphBuilder builder = ut::GraphBuilder("subgraph");
  auto data = builder.AddNode("sub_Data", "sub_Data", 0, 1);
  auto netoutput = builder.AddNode("sub_Netoutput", "sub_NetOutput", 1, 0);
  builder.AddDataEdge(data, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  return graph;
}

// construct graph which contains subgraph
static ComputeGraphPtr BuildComputeGraph() {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto data = builder.AddNode("Data", "Data", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  transdata->GetOpDesc()->AddSubgraphName("subgraph");
  transdata->GetOpDesc()->SetSubgraphInstanceName(0, "subgraph");
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  builder.AddDataEdge(data, 0, transdata, 0);
  builder.AddDataEdge(transdata, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  // add subgraph
  transdata->SetOwnerComputeGraph(graph);
  ComputeGraphPtr subgraph = BuildSubComputeGraph();
  subgraph->SetParentGraph(graph);
  subgraph->SetParentNode(transdata);
  graph->AddSubgraph("subgraph", subgraph);
  return graph;
}

TEST_F(UtestGraph, copy_graph_01) {
  ge::OpDescPtr add_op(new ge::OpDesc("add1", "Add"));
  add_op->AddDynamicInputDesc("input", 2);
  add_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  auto graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);
  ge::Graph copy_graph("copy_graph");
  ASSERT_EQ(copy_graph.CopyFrom(graph), ge::GRAPH_SUCCESS);
  Graph graph2("graph2");
  ASSERT_EQ(copy_graph.CopyFrom(graph2), GRAPH_FAILED);

  auto cp_compute_graph = ge::GraphUtilsEx::GetComputeGraph(copy_graph);
  ASSERT_NE(cp_compute_graph, nullptr);
  ASSERT_NE(cp_compute_graph, compute_graph);
  ASSERT_EQ(cp_compute_graph->GetDirectNodesSize(), 1);
  auto cp_add_node = cp_compute_graph->FindNode("add1");
  ASSERT_NE(cp_add_node, nullptr);
  ASSERT_NE(cp_add_node, add_node);
}

TEST_F(UtestGraph, copy_graph_02) {
  ge::OpDescPtr if_op(new ge::OpDesc("if", "If"));
  if_op->AddDynamicInputDesc("input", 1);
  if_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto if_node = compute_graph->AddNode(if_op);
  auto graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);
  ge::Graph copy_graph("copy_graph");

  if_op->AddSubgraphName("then_branch");
  if_op->AddSubgraphName("else_branch");
  if_op->SetSubgraphInstanceName(0, "then");
  if_op->SetSubgraphInstanceName(1, "else");

  ge::OpDescPtr add_op1(new ge::OpDesc("add1", "Add"));
  add_op1->AddDynamicInputDesc("input", 2);
  add_op1->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> then_compute_graph(new ge::ComputeGraph("then"));
  auto add_node1 = then_compute_graph->AddNode(add_op1);
  then_compute_graph->SetParentNode(if_node);
  then_compute_graph->SetParentGraph(compute_graph);
  compute_graph->AddSubgraph(then_compute_graph);

  ge::OpDescPtr add_op2(new ge::OpDesc("add2", "Add"));
  add_op2->AddDynamicInputDesc("input", 2);
  add_op2->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> else_compute_graph(new ge::ComputeGraph("else"));
  auto add_node2 = else_compute_graph->AddNode(add_op2);
  else_compute_graph->SetParentNode(if_node);
  else_compute_graph->SetParentGraph(compute_graph);
  compute_graph->AddSubgraph(else_compute_graph);

  ASSERT_EQ(copy_graph.CopyFrom(graph), ge::GRAPH_SUCCESS);

  auto cp_compute_graph = ge::GraphUtilsEx::GetComputeGraph(copy_graph);
  ASSERT_NE(cp_compute_graph, nullptr);
  ASSERT_NE(cp_compute_graph, compute_graph);
  ASSERT_EQ(cp_compute_graph->GetDirectNodesSize(), 1);
  auto cp_if_node = cp_compute_graph->FindNode("if");
  ASSERT_NE(cp_if_node, nullptr);
  ASSERT_NE(cp_if_node, if_node);

  auto cp_then_compute_graph = cp_compute_graph->GetSubgraph("then");
  ASSERT_NE(cp_then_compute_graph, nullptr);
  ASSERT_NE(cp_then_compute_graph, then_compute_graph);
  ASSERT_EQ(cp_then_compute_graph->GetDirectNodesSize(), 1);
  auto cp_add_node1 = cp_then_compute_graph->FindNode("add1");
  ASSERT_NE(cp_add_node1, nullptr);
  ASSERT_NE(cp_add_node1, add_node1);

  auto cp_else_compute_graph = cp_compute_graph->GetSubgraph("else");
  ASSERT_NE(cp_else_compute_graph, nullptr);
  ASSERT_NE(cp_else_compute_graph, else_compute_graph);
  ASSERT_EQ(cp_else_compute_graph->GetDirectNodesSize(), 1);
  auto cp_add_node2 = cp_else_compute_graph->FindNode("add2");
  ASSERT_NE(cp_add_node2, nullptr);
  ASSERT_NE(cp_add_node2, add_node2);
}

REG_OP(Mul)
    .OP_END_FACTORY_REG(Mul)
IMPL_INFER_VALUE_RANGE_FUNC(Mul, func){
  std::cout << "test" << std::endl;
  return GRAPH_SUCCESS;
}

REG_OP(Test2)
    .OP_END_FACTORY_REG(Test2)
IMPL_INFER_VALUE_RANGE_FUNC(Test2, func2){
  std::cout << "test" << std::endl;
  return GRAPH_SUCCESS;
}

TEST_F(UtestGraph, test_infer_value_range_register_succ) {
  string op_type = "Add";
  INFER_VALUE_RANGE_DEFAULT_REG(Add);
  INFER_VALUE_RANGE_DEFAULT_REG(Test1);
  auto para = OperatorFactoryImpl::GetInferValueRangePara(op_type);
  ASSERT_EQ(para.is_initialized, true);
  ASSERT_EQ(para.infer_value_func, nullptr);

  op_type = "Mul";
  INFER_VALUE_RANGE_CUSTOM_FUNC_REG(Mul, INPUT_HAS_VALUE_RANGE, func);
  INFER_VALUE_RANGE_CUSTOM_FUNC_REG(Test2, INPUT_IS_DYNAMIC, func2);
  para = OperatorFactoryImpl::GetInferValueRangePara(op_type);
  ASSERT_EQ(para.is_initialized, true);
  ASSERT_NE(para.infer_value_func, nullptr);

  op_type = "Sub";
  para = OperatorFactoryImpl::GetInferValueRangePara(op_type);
  ASSERT_EQ(para.is_initialized, false);
}

TEST_F(UtestGraph, IsRefFromRefData_HasNoAttr_ReturnFalse) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto ref_data = builder.AddNode("ref_data", "RefData", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  builder.AddDataEdge(ref_data, 0, transdata, 0);
  builder.AddDataEdge(transdata, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  auto out_data_anchor = transdata->GetOutDataAnchor(0);
  ASSERT_NE(out_data_anchor, nullptr);

  NodePtr node = nullptr;
  EXPECT_FALSE(GraphUtils::IsRefFromRefData(out_data_anchor, node));
}

TEST_F(UtestGraph, IsRefFromRefData_VarNameNotExist_ReturnFalse) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto ref_data = builder.AddNode("ref_data", "RefData", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  builder.AddDataEdge(ref_data, 0, transdata, 0);
  builder.AddDataEdge(transdata, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  ge::AttrUtils::SetStr(transdata->GetOpDesc()->MutableOutputDesc(0), "ref_var_src_var_name", "not_exist");
  auto out_data_anchor = transdata->GetOutDataAnchor(0);
  ASSERT_NE(out_data_anchor, nullptr);

  NodePtr node = nullptr;
  EXPECT_FALSE(GraphUtils::IsRefFromRefData(out_data_anchor, node));
}

TEST_F(UtestGraph, IsRefFromRefData_VarNameNodeIsNotRefData_ReturnFalse) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto ref_data = builder.AddNode("ref_data", "RefData", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  builder.AddDataEdge(ref_data, 0, transdata, 0);
  builder.AddDataEdge(transdata, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  ge::AttrUtils::SetStr(transdata->GetOpDesc()->MutableOutputDesc(0), "ref_var_src_var_name", "NetOutput");
  auto out_data_anchor = transdata->GetOutDataAnchor(0);
  ASSERT_NE(out_data_anchor, nullptr);

  NodePtr node = nullptr;
  EXPECT_FALSE(GraphUtils::IsRefFromRefData(out_data_anchor, node));
}

TEST_F(UtestGraph, IsRefFromRefData_ReturnTrue) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto ref_data = builder.AddNode("ref_data", "RefData", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  builder.AddDataEdge(ref_data, 0, transdata, 0);
  builder.AddDataEdge(transdata, 0, netoutput, 0);
  auto graph = builder.GetGraph();
  ge::AttrUtils::SetStr(transdata->GetOpDesc()->MutableOutputDesc(0), "ref_var_src_var_name", "ref_data");
  auto out_data_anchor = transdata->GetOutDataAnchor(0);
  ASSERT_NE(out_data_anchor, nullptr);

  NodePtr node = nullptr;
  EXPECT_TRUE(GraphUtils::IsRefFromRefData(out_data_anchor, node));
}

REG_OP(Shape)
    .OP_END_FACTORY_REG(Shape)
IMPL_INFER_VALUE_RANGE_FUNC(Shape, ShapeValueInfer){
  auto op_desc = OpDescUtils::GetOpDescFromOperator(op);
  auto output_tensor_desc = op_desc->MutableOutputDesc(0);
  std::vector<std::pair<int64_t, int64_t>> in_shape_range;
  op_desc->MutableInputDesc(0)->GetShapeRange(in_shape_range);
  if (!in_shape_range.empty()) {
    output_tensor_desc->SetValueRange(in_shape_range);
  }
  return GRAPH_SUCCESS;
}

TEST_F(UtestGraph, test_value_range_infer_and_set_get) {
  using std::make_pair;
  string op_type = "Shape";
  INFER_VALUE_RANGE_CUSTOM_FUNC_REG(Shape, INPUT_IS_DYNAMIC, ShapeValueInfer);
  auto graph = std::make_shared<ComputeGraph>("test_graph");
  auto shape_op_desc = std::make_shared<OpDesc>("node_name", op_type);
  GeTensorDesc tensor_desc(GeShape({-1, -1, 4, 192}), ge::FORMAT_NCHW, DT_INT32);
  std::vector<std::pair<int64_t, int64_t>> shape_range = {make_pair(1, 100), make_pair(1, 240),
                                                          make_pair(4, 4),   make_pair(192, 192)};
  tensor_desc.SetShapeRange(shape_range);
  shape_op_desc->AddInputDesc(tensor_desc);
  GeTensorDesc out_tensor_desc(GeShape({4}), ge::FORMAT_NCHW, DT_INT32);
  shape_op_desc->AddOutputDesc(out_tensor_desc);
  auto shape_node = graph->AddNode(shape_op_desc);
  Operator op = OpDescUtils::CreateOperatorFromNode(shape_node);
  auto ret = OpDescUtilsEx::CallInferValueRangeFunc(shape_node->GetOpDesc(), op);
  ASSERT_EQ(ret, GRAPH_SUCCESS);

  auto output_0_desc = shape_node->GetOpDesc()->GetOutputDesc(0);
  std::vector<std::pair<int64_t, int64_t>> value_range;
  output_0_desc.GetValueRange(value_range);
  EXPECT_EQ(value_range.size(), 4);

  std::vector<int64_t> target_value_range = {1, 100, 1, 240, 4, 4, 192, 192};
  std::vector<int64_t> output_value_range;
  for (auto pair : value_range) {
    output_value_range.push_back(pair.first);
    output_value_range.push_back(pair.second);
  }
  EXPECT_EQ(target_value_range, output_value_range);
}

TEST_F(UtestGraph, get_all_graph_nodes) {
  ComputeGraphPtr graph = BuildComputeGraph();
  auto nodes = graph->GetAllNodes();
  EXPECT_EQ(nodes.size(), 5);

  Graph graph2("Test");
  auto nodes_empty = graph2.GetAllNodes();
  EXPECT_EQ(nodes_empty.size(), 0);
}

TEST_F(UtestGraph, SetOutputs_ops) {
  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  std::vector<ge::Operator> outputs = {op1, op2, op3};

  Graph graph;
  graph.SetOutputs(outputs);
  EXPECT_EQ(graph.GetAllNodes().size(), 0);
  // EXPECT_TRUE(graph.impl_->output_name_.empty()); // impl缺少头文件，找不到声明
}

TEST_F(UtestGraph, SetOutputs_string) {
  using std::make_pair;
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  add_op->AddDynamicInputDesc("input", 2);
  add_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);

  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  std::string op_n1 = std::string("add");
  std::string op_n2 = std::string("op2");
  std::string op_n3 = std::string("op3");

  std::vector<std::pair<Operator, std::string>> outputs = {make_pair(op1, op_n1), make_pair(op2, op_n2),
                                                          make_pair(op3, op_n3)};
  graph.SetOutputs(outputs);
  EXPECT_EQ(graph.GetAllNodes().size(), 1);
}

TEST_F(UtestGraph, SetOutputs_AscendString) {
  using std::make_pair;
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  add_op->AddDynamicInputDesc("input", 2);
  add_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);

  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  AscendString op_n1 = AscendString("add");
  AscendString op_n2 = AscendString("op2");
  AscendString op_n3 = AscendString("op3");

  std::vector<std::pair<Operator, AscendString>> outputs = {make_pair(op1, op_n1), make_pair(op2, op_n2),
                                                          make_pair(op3, op_n3)};
  graph.SetOutputs(outputs);
  EXPECT_EQ(graph.GetAllNodes().size(), 1);
}

TEST_F(UtestGraph, SetOutputs_Index) {
  using std::make_pair;
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  add_op->AddDynamicInputDesc("input", 2);
  add_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);
  Graph graph2;

  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  std::vector<size_t> vec_index1 = {0,1,2};
  std::vector<size_t> vec_index2 = {0};
  std::vector<size_t> vec_index3 = {0};

  std::vector<std::pair<Operator, std::vector<size_t>>> outputs = {make_pair(op1, vec_index1),
    make_pair(op2, vec_index2),  make_pair(op3, vec_index3)};
  graph2.SetOutputs(outputs);
  graph.SetOutputs(outputs);
  EXPECT_EQ(graph.GetAllNodes().size(), 1);
}

TEST_F(UtestGraph, SetTargets) {
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  add_op->AddDynamicInputDesc("input", 2);
  add_op->AddDynamicOutputDesc("output", 1);
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);
  Graph graph2;

  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  std::vector<size_t> vec_index1 = {0,1,2};
  std::vector<size_t> vec_index2 = {0};
  std::vector<size_t> vec_index3 = {0};

  std::vector<ge::Operator> targets = {op1, op2, op3};

  graph2.SetTargets(targets);
  graph.SetTargets(targets);
  EXPECT_EQ(graph.GetAllNodes().size(), 1);
}

TEST_F(UtestGraph, SetNeedIteration) {
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);
  Graph graph2;

  graph2.SetNeedIteration(true);
  graph.SetNeedIteration(false);
  EXPECT_EQ(graph.GetAllNodes().size(), 1);
}

TEST_F(UtestGraph, GetDirectNode) {
  ge::OpDescPtr add_op(new ge::OpDesc("add_0", "add"));
  std::shared_ptr<ge::ComputeGraph> compute_graph(new ge::ComputeGraph("test_graph"));
  auto add_node = compute_graph->AddNode(add_op);
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph);

  ge::OpDescPtr add_op2(new ge::OpDesc("add_1", "add"));
  std::shared_ptr<ge::ComputeGraph> compute_graph2 = nullptr;
  Graph graph2 = ge::GraphUtilsEx::CreateGraphFromComputeGraph(compute_graph2);

  Graph graph3;

  std::vector<GNode> gnodes, gnodes2, gnodes3;

  gnodes = graph.GetDirectNode();
  gnodes2 = graph2.GetDirectNode();
  gnodes3 = graph3.GetDirectNode();
  EXPECT_EQ(gnodes.size(), 1);
}

TEST_F(UtestGraph, RemoveNode) {
  ComputeGraphPtr cgp = BuildComputeGraph();
  auto v_nodes = cgp->GetAllNodes();
  EXPECT_EQ(v_nodes.size(), 5);

  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  auto nodes = graph.GetAllNodes();
  graph.RemoveNode(nodes[4]);
  EXPECT_EQ(graph.GetAllNodes().size(), 4);

  graph.RemoveNode(nodes[0], true);
  EXPECT_EQ(graph.GetAllNodes().size(), 3);
}

TEST_F(UtestGraph, AddRemoveEdge1) {
  Operator op1 = Operator("add");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");

  Graph graph("a_graph");
  Graph graph2;

  GNode node1 = graph.AddNodeByOp(op1);
  GNode node2 = graph.AddNodeByOp(op2);
  GNode node3 = graph.AddNodeByOp(op3);

  auto ret =graph.AddDataEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph.AddControlEdge(node2, node3);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph.RemoveEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);

  graph2.AddNodeByOp(op1);
  ret =graph2.AddDataEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph2.AddControlEdge(node2, node3);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph2.RemoveEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);
}

TEST_F(UtestGraph, AddRemoveEdge2) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto data = builder.AddNode("Data", "Data", 0, 1);
  ComputeGraphPtr cgp = builder.GetGraph();
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  auto nodes = graph.GetAllNodes();
  EXPECT_EQ(nodes.size(), 1);

  GNode node1 = nodes[0];
  GNode node2;

  auto ret =graph.AddDataEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph.RemoveEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_FAILED);
  ret = graph.AddControlEdge(node1, node2);
  EXPECT_EQ(ret, GRAPH_FAILED);
}

TEST_F(UtestGraph, AddRemoveEdge3) {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto data = builder.AddNode("Data", "Data", 0, 1);
  auto transdata = builder.AddNode("Transdata", "Transdata", 1, 1);
  auto netoutput = builder.AddNode("Netoutput", "NetOutput", 1, 0);
  ComputeGraphPtr cgp = builder.GetGraph();
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  auto nodes = graph.GetAllNodes();
  EXPECT_EQ(nodes.size(), 3);

  GNode node1 = nodes[0];
  GNode node2 = nodes[1];
  GNode node3 = nodes[2];

  auto ret = graph.AddDataEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
  ret = graph.AddControlEdge(node2, node3);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
  ret = graph.RemoveEdge(node1, 0, node2, 0);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
}

TEST_F(UtestGraph, ConstructFromInputs1) {
  Graph graph;
  Operator op1 = Operator("op1");
  Operator op2 = Operator("op2");
  Operator op3 = Operator("op3");
  std::vector<Operator> inputs = {op1, op2, op3};
  AscendString name = "graph_name";

  auto ret = graph.ConstructFromInputs({}, name);
  EXPECT_EQ(ret, nullptr);

  ret = graph.ConstructFromInputs(inputs, AscendString(nullptr));
  EXPECT_EQ(ret, nullptr);

  ret = graph.ConstructFromInputs(inputs, name);
  EXPECT_EQ(ret, nullptr);
}

REG_OP(Phony0)
    .OUTPUT(y,
            TensorType({DT_FLOAT, DT_FLOAT16, DT_INT8, DT_INT16, DT_UINT16, DT_UINT8, DT_INT32, DT_INT64, DT_UINT32,
                        DT_UINT64, DT_BOOL, DT_DOUBLE}))
    .ATTR(value, Tensor, Tensor())
    .OP_END_FACTORY_REG(Phony0);

REG_OP(Phony1)
    .DYNAMIC_INPUT(x, TensorType::NumberType())
    .OUTPUT(y, TensorType::NumberType())
    .REQUIRED_ATTR(N, Int)
    .OP_END_FACTORY_REG(Phony1);

REG_OP(Phony2)
    .INPUT(x,
           TensorType({DT_FLOAT, DT_FLOAT16, DT_INT8, DT_INT16, DT_UINT16, DT_UINT8, DT_INT32, DT_INT64, DT_UINT32,
                       DT_UINT64, DT_BOOL, DT_DOUBLE}))
    .INPUT(shape, TensorType({DT_INT32, DT_INT64}))
    .OUTPUT(y,
            TensorType({DT_FLOAT, DT_FLOAT16, DT_INT8, DT_INT16, DT_UINT16, DT_UINT8, DT_INT32, DT_INT64, DT_UINT32,
                        DT_UINT64, DT_BOOL, DT_DOUBLE}))
    .ATTR(axis, Int, 0)
    .ATTR(num_axes, Int, -1)
    .OP_END_FACTORY_REG(Phony2);

TEST_F(UtestGraph, ConstructFromInputs2) {
  Graph graph;
  Operator op1 = op::Phony0("op1");
  Operator op2 = op::Phony1("op2");
  Operator op3 = op::Phony2("op3");
  std::vector<Operator> inputs = {op1, op2, op3};
  AscendString name = "graph_name";

  auto ret = graph.ConstructFromInputs(inputs, name);
  EXPECT_NE(ret, nullptr);
}

TEST_F(UtestGraph, SaveLoadFile) {
  system("rm -rf ./ut_graph1.txt");
  system("rm -rf ./ut_graph2.txt");

  ComputeGraphPtr cgp = BuildComputeGraph();
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  auto ret = graph.SaveToFile(nullptr);
  EXPECT_EQ(ret, GRAPH_FAILED);

  ret = graph.SaveToFile("./ut_graph1.txt");
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  ret = graph.SaveToFile(std::string("./ut_graph2.txt"));
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  Graph graph2;
  ret = graph2.LoadFromFile(nullptr);
  EXPECT_EQ(ret, GRAPH_FAILED);

  Graph graph3;
  ret = graph3.LoadFromFile("./ut_graph1.txt");
  EXPECT_NE(ret, GRAPH_FAILED);

  Graph graph4;
  ret = graph4.LoadFromFile(std::string("./ut_graph2.txt"));
  EXPECT_NE(ret, GRAPH_FAILED);
}

TEST_F(UtestGraph, LoadFromSerializedModelArray_InvalidParams) {
  ge::proto::ModelDef model_def;
  auto *graph_def = model_def.add_graph();
  graph_def->set_name("serialized_model_array_graph");

  Graph graph;
  EXPECT_NE(graph.LoadFromSerializedModelArray(nullptr, 0), GRAPH_SUCCESS);

  std::string serialized;
  EXPECT_NE(graph.LoadFromSerializedModelArray(reinterpret_cast<const uint8_t*>(serialized.c_str()), 0), GRAPH_SUCCESS);

  serialized = "abc";
  EXPECT_NE(graph.LoadFromSerializedModelArray(reinterpret_cast<const uint8_t*>(serialized.c_str()), serialized.size()), GRAPH_SUCCESS);
}


std::vector<std::string> CreateOpDef(ge::proto::GraphDef *def, const std::string &type, const std::vector<std::string> &inputs,
                                     size_t num_outputs, std::vector<std::string> subgraphs = {}) {
  auto name = type + std::to_string(def->op_size());
  auto *op_def = def->add_op();
  op_def->set_name(name);
  op_def->set_type(type);


  auto op_desc_attr = op_def->mutable_attr();
  proto::AttrDef input_desc_name;
  proto::AttrDef input_desc_index;
  proto::AttrDef output_desc_name;
  proto::AttrDef output_desc_index;

  for (size_t i = 0U; i < inputs.size(); ++i) {
    op_def->add_input_desc();
    *op_def->add_input() = inputs[i];

    input_desc_name.mutable_list()->add_s(std::string("x") + std::to_string(i));
    input_desc_index.mutable_list()->add_i(i);
  }
  std::vector<std::string> outputs;
  for (size_t i = 0U; i < num_outputs; ++i) {
    op_def->add_output_desc();
    outputs.push_back(op_def->name() + ":" + std::to_string(i));

    output_desc_name.mutable_list()->add_s(std::string("y") + std::to_string(i));
    output_desc_index.mutable_list()->add_i(i);
  }

  (void) op_desc_attr->insert({"_input_name_key", input_desc_name});
  (void) op_desc_attr->insert({"_input_name_value", input_desc_index});

  (void) op_desc_attr->insert({"_output_name_key", output_desc_name});
  (void) op_desc_attr->insert({"_output_name_value", output_desc_index});

  for (auto &subgraph : subgraphs) {
      op_def->add_subgraph_name(subgraph);
  }

  if (num_outputs == 0) {
    outputs.push_back(op_def->name());
  }

  return outputs;
}


std::string GetStringBeforeColon(const std::string& str) {
    size_t pos = str.find(':');
    if (pos != std::string::npos) {
        return str.substr(0, pos);
    } else {
        return str;
    }
}


void AssertOpMatch(ge::ComputeGraphPtr &compute_graph, const std::vector<std::string> &op,
                   const std::vector<std::string> &inputs, size_t num_outputs) {
  auto op_name = GetStringBeforeColon(op[0]);
  auto data = compute_graph->FindNode(op_name);
  ASSERT_NE(data, nullptr);
  ASSERT_EQ(data->GetInDataNodesAndAnchors().size(), inputs.size());
  size_t index = 0U;
  for (auto &node_and_anchor : data->GetInDataNodesAndAnchors()) {
    auto input = node_and_anchor.first->GetName() + ":" + std::to_string(node_and_anchor.second->GetIdx());
    ASSERT_EQ(input, inputs[index]);
    index++;
  }
  auto in_name_idx = data->GetOpDesc()->GetAllInputName();
  ASSERT_EQ(in_name_idx.size(), inputs.size());
  index = 0U;
  for (auto &name_idx : in_name_idx) {
    ASSERT_EQ(name_idx.first, "x" + std::to_string(index));
    ASSERT_EQ(name_idx.second, index);
    index++;
  }
  auto out_name_idx = data->GetOpDesc()->GetAllOutputName();
  ASSERT_EQ(out_name_idx.size(), num_outputs);
  index = 0U;
  for (auto &name_idx : out_name_idx) {
    ASSERT_EQ(name_idx.first, "y" + std::to_string(index));
    ASSERT_EQ(name_idx.second, index);
    index++;
  }
}


TEST_F(UtestGraph, LoadFromSerializedModelArray_NoSubGraph) {
  ge::proto::ModelDef model_def;
  auto *graph_def = model_def.add_graph();
  graph_def->set_name("root_graph");

  auto data = CreateOpDef(graph_def, "Data", {}, 1);
  auto abs = CreateOpDef(graph_def, "Abs", data, 1);
  auto sqrt = CreateOpDef(graph_def, "Add", {data[0], abs[0]}, 1);
  auto netoutput = CreateOpDef(graph_def, "NetOutput", {abs[0], sqrt[0]}, 0);

  Graph graph;
  auto serialized = model_def.SerializeAsString();
  ASSERT_EQ(graph.LoadFromSerializedModelArray(serialized.c_str(), serialized.size()), GRAPH_SUCCESS);

  auto compute_graph = ge::GraphUtilsEx::GetComputeGraph(graph);
  ASSERT_EQ(compute_graph->GetName(), graph_def->name());

  AssertOpMatch(compute_graph, data, {}, 1);
  AssertOpMatch(compute_graph, abs, data, 1);
  AssertOpMatch(compute_graph, sqrt, {data[0], abs[0]}, 1);
  AssertOpMatch(compute_graph, netoutput, {abs[0], sqrt[0]}, 0);
}

TEST_F(UtestGraph, LoadFromSerializedModelArray_WithSubGraph) {
  ge::proto::ModelDef model_def;
  auto *graph_def = model_def.add_graph();
  graph_def->set_name("root_graph");
  auto func = CreateOpDef(graph_def, "FuncOp", {}, 0, {"sub_graph"});

  auto *sub_graph = model_def.add_graph();
  sub_graph->set_name("sub_graph");
  auto data = CreateOpDef(sub_graph, "Data", {}, 1);
  auto abs = CreateOpDef(sub_graph, "Abs", data, 1);
  auto sqrt = CreateOpDef(sub_graph, "Add", {data[0], abs[0]}, 1);
  auto netoutput = CreateOpDef(sub_graph, "NetOutput", {abs[0], sqrt[0]}, 0);

  Graph graph;
  auto serialized = model_def.SerializeAsString();
  ASSERT_EQ(graph.LoadFromSerializedModelArray(serialized.c_str(), serialized.size()), GRAPH_SUCCESS);

  auto compute_graph = ge::GraphUtilsEx::GetComputeGraph(graph);
  ASSERT_EQ(compute_graph->GetName(), graph_def->name());

  ASSERT_EQ(compute_graph->GetAllSubgraphs().size(), 1U);
  auto sub_compute_graph = compute_graph->GetSubgraph("sub_graph");
  ASSERT_NE(sub_compute_graph, nullptr);
  ASSERT_EQ(sub_compute_graph->GetName(), "sub_graph");

  auto func_op = compute_graph->FindNode(GetStringBeforeColon(func[0]));
  ASSERT_NE(func_op, nullptr);
  ASSERT_EQ(sub_compute_graph->GetParentNode(), func_op);
  ASSERT_EQ(sub_compute_graph->GetParentGraph(), compute_graph);

  AssertOpMatch(sub_compute_graph, data, {}, 1);
  AssertOpMatch(sub_compute_graph, abs, data, 1);
  AssertOpMatch(sub_compute_graph, sqrt, {data[0], abs[0]}, 1);
  AssertOpMatch(sub_compute_graph, netoutput, {abs[0], sqrt[0]}, 0);
}

TEST_F(UtestGraph, LoadFromSerializedModelArray_WithNestedSubGraph) {
  ge::proto::ModelDef model_def;
  auto *graph_def = model_def.add_graph();
  graph_def->set_name("root_graph");
  auto func = CreateOpDef(graph_def, "FuncOp", {}, 0, {"sub_graph"});

  auto *sub_graph0 = model_def.add_graph();
  sub_graph0->set_name("sub_graph");
  auto func1 = CreateOpDef(sub_graph0, "FuncOp1", {}, 0, {"sub_graph1"});

  auto *sub_graph1 = model_def.add_graph();
  sub_graph1->set_name("sub_graph1");
  auto data = CreateOpDef(sub_graph1, "Data", {}, 1);
  auto abs = CreateOpDef(sub_graph1, "Abs", data, 1);
  auto sqrt = CreateOpDef(sub_graph1, "Add", {data[0], abs[0]}, 1);
  auto netoutput = CreateOpDef(sub_graph1, "NetOutput", {abs[0], sqrt[0]}, 0);

  Graph graph;
  auto serialized = model_def.SerializeAsString();
  ASSERT_EQ(graph.LoadFromSerializedModelArray(serialized.c_str(), serialized.size()), GRAPH_SUCCESS);

  auto compute_graph = ge::GraphUtilsEx::GetComputeGraph(graph);
  ASSERT_EQ(compute_graph->GetName(), graph_def->name());

  ASSERT_EQ(compute_graph->GetAllSubgraphs().size(), 2U);
  auto sub_compute_graph = compute_graph->GetSubgraph("sub_graph");
  ASSERT_NE(sub_compute_graph, nullptr);
  ASSERT_EQ(sub_compute_graph->GetName(), "sub_graph");

  auto sub_compute_graph1 = compute_graph->GetSubgraph("sub_graph1");
  ASSERT_NE(sub_compute_graph1, nullptr);
  ASSERT_EQ(sub_compute_graph1->GetName(), "sub_graph1");

  auto func_op = compute_graph->FindNode(GetStringBeforeColon(func[0]));
  ASSERT_NE(func_op, nullptr);
  ASSERT_EQ(sub_compute_graph->GetParentNode(), func_op);
  ASSERT_EQ(sub_compute_graph->GetParentGraph(), compute_graph);

  auto func_op1 = sub_compute_graph->FindNode(GetStringBeforeColon(func1[0]));
  ASSERT_NE(func_op1, nullptr);
  ASSERT_EQ(sub_compute_graph1->GetParentNode(), func_op1);
  ASSERT_EQ(sub_compute_graph1->GetParentGraph(), sub_compute_graph);

  AssertOpMatch(sub_compute_graph1, data, {}, 1);
  AssertOpMatch(sub_compute_graph1, abs, data, 1);
  AssertOpMatch(sub_compute_graph1, sqrt, {data[0], abs[0]}, 1);
  AssertOpMatch(sub_compute_graph1, netoutput, {abs[0], sqrt[0]}, 0);
}

TEST_F(UtestGraph, SaveAndLoadMemWithBuffer) {
  ComputeGraphPtr cgp = BuildComputeGraph();
  Graph graph1 = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  GraphBuffer buf1;
  auto ret = graph1.SaveToMem(buf1);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  Graph graph2;
  ret = graph2.LoadFromMem(buf1);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  GraphBuffer buf2;
  ret = graph2.SaveToMem(buf2);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  EXPECT_EQ(buf1.GetSize(), buf2.GetSize());
  EXPECT_EQ(memcmp(buf1.GetData(), buf2.GetData(), buf1.GetSize()), 0);
}

TEST_F(UtestGraph, SaveAndLoadMemWithData) {
  ComputeGraphPtr cgp = BuildComputeGraph();
  Graph graph1 = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  GraphBuffer buf1;
  auto ret = graph1.SaveToMem(buf1);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  Graph graph2;
  ret = graph2.LoadFromMem(buf1.GetData(), buf1.GetSize());
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  GraphBuffer buf2;
  ret = graph2.SaveToMem(buf2);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  EXPECT_EQ(buf1.GetSize(), buf2.GetSize());
  EXPECT_EQ(memcmp(buf1.GetData(), buf2.GetData(), buf1.GetSize()), 0);
}

TEST_F(UtestGraph, LoadFromMemFailed) {
  GraphBuffer buf;
  Graph graph;
  auto ret = graph.LoadFromMem(buf.GetData(), buf.GetSize());
  EXPECT_NE(ret, GRAPH_SUCCESS);

  ret = graph.LoadFromMem(nullptr, 0);
  EXPECT_NE(ret, GRAPH_SUCCESS);
}

TEST_F(UtestGraph, ErrorCodeCheck) {
  EXPECT_EQ(ge::FAILED, 4294967295);
  EXPECT_EQ(ge::END_OF_SEQUENCE, 1343225863);
  EXPECT_EQ(ge::GE_GRAPH_SAVE_WEIGHTS_FAILED, 1343242286);

  EXPECT_EQ(strcmp(GE_GET_ERRORNO_STR(ge::END_OF_SEQUENCE).c_str(), "End of sequence!"), 0);
  EXPECT_EQ(strcmp(GE_GET_ERRORNO_STR(ge::FAILED).c_str(), "failed"), 0);
  EXPECT_EQ(strcmp(GE_GET_ERRORNO_STR(ge::GE_GRAPH_SAVE_WEIGHTS_FAILED).c_str(),
    "OMG Save Weights to Model failed."), 0);
}

TEST_F(UtestGraph, GetName) {
  Graph graph;
  AscendString name;
  auto ret = graph.GetName(name);
  EXPECT_EQ(ret, GRAPH_FAILED);
}

TEST_F(UtestGraph, RecoverGraphOperators) {
  ComputeGraphPtr cgp = BuildComputeGraph();
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);
  auto ret = GraphUtilsEx::RecoverGraphOperators(graph);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
}

TEST_F(UtestGraph, GetOpName) {
  ComputeGraphPtr cgp = BuildComputeGraph();
  Graph graph = ge::GraphUtilsEx::CreateGraphFromComputeGraph(cgp);

  Operator op1("add");
  auto ret = graph.AddOp(op1);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  std::vector<std::string> op_names1;
  ret = graph.GetAllOpName(op_names1);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  std::vector<AscendString> op_names2;
  ret = graph.GetAllOpName(op_names2);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
}

TEST_F(UtestGraph, FindOpByName) {
  Graph graph;
  Operator op1 = op::Phony0("op1");
  Operator op2 = op::Phony1("op2");
  Operator op3 = op::Phony2("op3");
  std::vector<Operator> inputs = {op1, op2, op3};
  AscendString name = "graph_name";

  GraphPtr gptr = Graph::ConstructFromInputs(inputs, name);

  EXPECT_EQ(gptr->GetAllNodes().size(), 2);

  Operator op1_2;
  auto ret = gptr->FindOpByName(nullptr, op1_2);
  ret = gptr->FindOpByName("op1", op1_2);
  EXPECT_EQ(ret, GRAPH_FAILED);

  Operator op2_2;
  ret = gptr->FindOpByName(std::string("op2"), op2_2);
  EXPECT_EQ(ret, GRAPH_FAILED);
}

TEST_F(UtestGraph, FindOpByType) {
  Graph graph;
  Operator op1 = op::Phony0("op1");
  Operator op2 = op::Phony1("op2");
  Operator op3 = op::Phony2("op3");
  std::vector<Operator> inputs = {op1, op2, op3};
  AscendString name = "graph_name";

  GraphPtr gptr = Graph::ConstructFromInputs(inputs, name);

  std::vector<ge::Operator> op1_2;
  auto ret = gptr->FindOpByType(nullptr, op1_2);
  ret = gptr->FindOpByType("const", op1_2);
  EXPECT_EQ(ret, GRAPH_SUCCESS);

  std::vector<ge::Operator> op2_2;
  ret = gptr->FindOpByType(std::string("data"), op2_2);
  EXPECT_EQ(ret, GRAPH_SUCCESS);
}

TEST_F(UtestGraph, SaveInvalidPath) {
  std::vector<Operator> inputs{};
  std::vector<Operator> outputs{};
  Graph graph("empty_graph");
  graph.SetInputs(inputs).SetOutputs(outputs);
  std::string file_name = "....1263713612～";
  EXPECT_EQ(graph.SaveToFile(file_name), GRAPH_FAILED);
}

