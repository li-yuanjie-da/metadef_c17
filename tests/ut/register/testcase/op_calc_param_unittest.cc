/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "register/op_ext_calc_param_registry.h"
#include "proto/task.pb.h"
#include <gtest/gtest.h>

class OpExtCalcParamRegistryUnittest : public testing::Test {};

namespace TestOpExtCalcParamRegistry {
ge::Status OpExtCalcParam(const ge::Node &node) {
  return 0;
}

TEST_F(OpExtCalcParamRegistryUnittest, OpExtCalcParamRegisterSuccess_Test) {
  EXPECT_EQ(fe::OpExtCalcParamRegistry::GetInstance().FindRegisterFunc("test"), nullptr);
  REGISTER_NODE_EXT_CALC_PARAM("test", OpExtCalcParam);
  EXPECT_EQ(fe::OpExtCalcParamRegistry::GetInstance().FindRegisterFunc("test"), OpExtCalcParam);
}
}
