/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "register/stream_manage_func_registry.h"

namespace ge {
uint32_t FakeCallbackFunc(MngActionType action_type, MngResourceHandle handle) {
  (void) action_type;
  (void) handle;
  return SUCCESS;
}

uint32_t FakeCallbackFuncFailed(MngActionType action_type, MngResourceHandle handle) {
  (void) action_type;
  (void) handle;
  return FAILED;
}

class StreamMngFuncRegistryUT : public testing::Test {};

TEST_F(StreamMngFuncRegistryUT, TryCallStreamMngFunc_Ok) {
  const auto func_type = StreamMngFuncType::ACLNN_STREAM_CALLBACK;
  const MngActionType action_type = MngActionType::DESTROY_STREAM;
  const MngResourceHandle handle = {.stream = (void *) 0x11};
  // case: no callback function is registered
  EXPECT_EQ(StreamMngFuncRegistry::GetInstance().TryCallStreamMngFunc(func_type, action_type, handle), SUCCESS);

  // case: run callback function successfully
  REG_STREAM_MNG_FUNC(func_type, FakeCallbackFunc);
  EXPECT_EQ(StreamMngFuncRegistry::GetInstance().TryCallStreamMngFunc(func_type, action_type, handle), SUCCESS);
}

TEST_F(StreamMngFuncRegistryUT, TryCallStreamMngFunc_Ok_CallFuncReturnNonzero) {
  const auto func_type = StreamMngFuncType::ACLNN_STREAM_CALLBACK;
  const MngActionType action_type = MngActionType::RESET_DEVICE;
  const MngResourceHandle handle = {.device_id = 0};
  // case: run callback function failed
  REG_STREAM_MNG_FUNC(func_type, FakeCallbackFuncFailed);
  EXPECT_EQ(StreamMngFuncRegistry::GetInstance().TryCallStreamMngFunc(func_type, action_type, handle), SUCCESS);
}

TEST_F(StreamMngFuncRegistryUT, TryCallStreamMngFunc_Ok_MultipleRegister) {
  const auto func_type = StreamMngFuncType::ACLNN_STREAM_CALLBACK;
  // register for the first time
  REG_STREAM_MNG_FUNC(func_type, FakeCallbackFuncFailed);
  EXPECT_EQ(StreamMngFuncRegistry::GetInstance().LookUpStreamMngFunc(func_type), FakeCallbackFuncFailed);
  // register for the second time
  REG_STREAM_MNG_FUNC(func_type, FakeCallbackFunc);
  EXPECT_EQ(StreamMngFuncRegistry::GetInstance().LookUpStreamMngFunc(func_type), FakeCallbackFunc);
}

}  // namespace ge