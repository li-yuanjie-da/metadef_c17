/**
 * Copyright 2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph/ge_local_context.h"
#include "nlohmann/json.hpp"
#include "common/ge_common/debug/ge_log.h"
#include <utility>

namespace ge {
using Json = nlohmann::json;

GEThreadLocalContext &GetThreadLocalContext() {
  static thread_local GEThreadLocalContext thread_context;
  return thread_context;
}

graphStatus GEThreadLocalContext::GetOption(const std::string &key, std::string &option) {
  const std::map<std::string, std::string>::const_iterator graph_iter = graph_options_.find(key);
  if (graph_iter != graph_options_.end()) {
    option = graph_iter->second;
    return GRAPH_SUCCESS;
  }
  const std::map<std::string, std::string>::const_iterator session_iter = session_options_.find(key);
  if (session_iter != session_options_.end()) {
    option = session_iter->second;
    return GRAPH_SUCCESS;
  }
  const std::map<std::string, std::string>::const_iterator global_iter = global_options_.find(key);
  if (global_iter != global_options_.end()) {
    option = global_iter->second;
    return GRAPH_SUCCESS;
  }
  return GRAPH_PARAM_INVALID;
}

void GEThreadLocalContext::SetGlobalOption(std::map<std::string, std::string> options_map) {
  global_options_.clear();
  global_options_ = std::move(options_map);
}

void GEThreadLocalContext::SetSessionOption(std::map<std::string, std::string> options_map) {
  session_options_.clear();
  session_options_ = std::move(options_map);
}

void GEThreadLocalContext::SetGraphOption(std::map<std::string, std::string> options_map) {
  graph_options_.clear();
  graph_options_ = std::move(options_map);
}

graphStatus GEThreadLocalContext::SetOptionNameMap(const std::string &option_name_map_json) {
  Json option_json;
  try {
    option_json = Json::parse(option_name_map_json);
  } catch (nlohmann::json::parse_error &) {
    GELOGE(ge::GRAPH_FAILED, "Parse JsonStr to Json failed, JsonStr: %s", option_name_map_json.c_str());
    return ge::GRAPH_FAILED;
  }
  for (auto iter : option_json.items()) {
    if (iter.key().empty()) {
      GELOGE(ge::GRAPH_FAILED, "Check option_name_map failed, key is null");
      return ge::GRAPH_FAILED;
    }
    if (static_cast<std::string>(iter.value()).empty()) {
      GELOGE(ge::GRAPH_FAILED, "Check option_name_map failed, value is null");
      return ge::GRAPH_FAILED;
    }
    option_name_map_.insert({iter.key(), static_cast<std::string>(iter.value())});
  }
  return ge::GRAPH_SUCCESS;
}

const std::string &GEThreadLocalContext::GetReadableName(const std::string &key) {
  auto iter = option_name_map_.find(key);
  if (iter != option_name_map_.end()) {
    GELOGD("Option %s's readable name is show name: %s", key.c_str(), iter->second.c_str());
    return iter->second;
  }
  GELOGD("Option %s's readable name is GE IR option: %s", key.c_str(), key.c_str());
  return key;
}

std::map<std::string, std::string> GEThreadLocalContext::GetAllGraphOptions() const {
  return graph_options_;
}
std::map<std::string, std::string> GEThreadLocalContext::GetAllSessionOptions() const {
  return session_options_;
}
std::map<std::string, std::string> GEThreadLocalContext::GetAllGlobalOptions() const {
  return global_options_;
}

std::map<std::string, std::string> GEThreadLocalContext::GetAllOptions() const {
  std::map<std::string, std::string> options_all;
  options_all.insert(graph_options_.cbegin(), graph_options_.cend());
  options_all.insert(session_options_.cbegin(), session_options_.cend());
  options_all.insert(global_options_.cbegin(), global_options_.cend());
  return options_all;
}
}  // namespace ge
