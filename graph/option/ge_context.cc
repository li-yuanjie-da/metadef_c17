/**
 * Copyright 2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph/ge_context.h"
#include "graph/ge_global_options.h"
#include "graph/ge_local_context.h"
#include "graph/types.h"
#include "common/ge_common/debug/ge_log.h"
#include "utils/extern_math_util.h"

namespace ge {
namespace {
const uint64_t kMinTrainingTraceJobId = 65536U;
const int32_t kDecimal = 10;
const char_t *kHostExecPlacement = "HOST";
const char_t *kEnabled = "1";

template<class T>
ge::Status GetOptionValue(const std::string &option_name, T &var) {
  std::string option;
  if (ge::GetContext().GetOption(option_name, option) != GRAPH_SUCCESS) {
    return ge::FAILED;
  }

  int64_t value = 0;
  try {
    value = static_cast<int64_t>(std::stoi(option.c_str()));
  } catch (std::invalid_argument &) {
    GELOGW("[Init] Transform option %s %s to int failed, as catching invalid_argument exception", option_name.c_str(),
           option.c_str());
    return ge::FAILED;
  } catch (std::out_of_range &) {
    GELOGW("[Init] Transform option %s %s to int failed, as catching out_of_range exception", option_name.c_str(),
           option.c_str());
    return ge::FAILED;
  }
  if (!IntegerChecker<T>::Compat(value)) {
    GELOGW("[Init] Transform option %s %s to int failed, value is invalid_argument", option_name.c_str(),
           option.c_str());
    return ge::FAILED;
  }
  var = value;
  return ge::SUCCESS;
}
}  // namespace

GEContext &GetContext() {
  static GEContext ge_context {};
  return ge_context;
}

thread_local uint64_t GEContext::session_id_ = 0UL;
thread_local uint64_t GEContext::context_id_ = 0UL;

graphStatus GEContext::GetOption(const std::string &key, std::string &option) {
  return GetThreadLocalContext().GetOption(key, option);
}

const std::string &GEContext::GetReadableName(const std::string &key) {
  return GetThreadLocalContext().GetReadableName(key);
}

bool GEContext::IsOverflowDetectionOpen() const {
  std::string enable_overflow_detection;
  if (GetThreadLocalContext().GetOption("ge.exec.overflow", enable_overflow_detection) != GRAPH_SUCCESS) {
    return false;
  }
  GELOGD("Option ge.exec.overflow is %s.", enable_overflow_detection.c_str());
  return (enable_overflow_detection == kEnabled);
}

bool GEContext::IsGraphLevelSat() const {
  std::string graph_level_sat;
  if (GetThreadLocalContext().GetOption("ge.graphLevelSat", graph_level_sat) != GRAPH_SUCCESS) {
    return false;
  }
  GELOGD("Option ge.graphLevelSat is %s.", graph_level_sat.c_str());
  return (graph_level_sat == kEnabled);
}

bool GEContext::GetHostExecFlag() const {
  std::string exec_placement;
  if (GetThreadLocalContext().GetOption("ge.exec.placement", exec_placement) != GRAPH_SUCCESS) {
    return false;
  }
  GELOGD("Option ge.exec.placement is %s.", exec_placement.c_str());
  return exec_placement == kHostExecPlacement;
}

bool GEContext::GetTrainGraphFlag() const {
  std::string run_mode;
  if ((GetThreadLocalContext().GetOption(ge::OPTION_GRAPH_RUN_MODE, run_mode) == ge::GRAPH_SUCCESS) &&
      (!run_mode.empty())) {
    const int32_t base = 10;
    if (static_cast<ge::GraphRunMode>(std::strtol(run_mode.c_str(), nullptr, base)) >= ge::TRAIN) {
      return true;
    }
  }
  return false;
}

std::mutex &GetGlobalOptionsMutex() {
  static std::mutex global_options_mutex;
  return global_options_mutex;
}

std::map<std::string, std::string> &GetMutableGlobalOptions() {
  static std::map<std::string, std::string> context_global_options{};
  return context_global_options;
}

void GEContext::Init() {
  (void) GetOptionValue("ge.exec.sessionId", session_id_);

  (void) GetOptionValue("ge.exec.deviceId", device_id_);

  std::string job_id;
  (void)GetOption("ge.exec.jobId", job_id);
  std::string s_job_id = "";
  for (const auto c : job_id) {
    if ((c >= '0') && (c <= '9')) {
      s_job_id += c;
    }
  }
  if (s_job_id == "") {
    trace_id_ = kMinTrainingTraceJobId;
    return;
  }
  const auto d_job_id = std::strtoll(s_job_id.c_str(), nullptr, kDecimal);
  if (static_cast<uint64_t>(d_job_id) < kMinTrainingTraceJobId) {
    trace_id_ = static_cast<uint64_t>(d_job_id) + kMinTrainingTraceJobId;
  } else {
    trace_id_ = static_cast<uint64_t>(d_job_id);
  }

  (void) GetOptionValue("stream_sync_timeout", stream_sync_timeout_);

  (void) GetOptionValue("event_sync_timeout", event_sync_timeout_);
}

uint64_t GEContext::SessionId() const { return session_id_; }

uint32_t GEContext::DeviceId() const {
  uint32_t device_id = 0U;
  // session device id has priority
  auto status = GetOptionValue("ge.session_device_id", device_id);
  return (status == ge::SUCCESS) ? device_id : device_id_;
}

int32_t GEContext::StreamSyncTimeout() const { return stream_sync_timeout_; }

int32_t GEContext::EventSyncTimeout() const { return event_sync_timeout_; }

void GEContext::SetSessionId(const uint64_t session_id) { session_id_ = session_id; }

void GEContext::SetContextId(const uint64_t context_id) { context_id_ = context_id; }

void GEContext::SetCtxDeviceId(const uint32_t device_id) { device_id_ = device_id; }

void GEContext::SetStreamSyncTimeout(const int32_t timeout) { stream_sync_timeout_ = timeout; }

void GEContext::SetEventSyncTimeout(const int32_t timeout) { event_sync_timeout_ = timeout; }

graphStatus GEContext::SetOptionNameMap(const std::string &option_name_map_json) {
  return GetThreadLocalContext().SetOptionNameMap(option_name_map_json);
}

void GEContext::SetMultiBatchShapeIndex(uint32_t graph_id,
    const std::map<int32_t, std::vector<int32_t>> &data_index_and_shape_map) {
  data_index_and_shape_map_[graph_id] = data_index_and_shape_map;
}

const std::map<int32_t, std::vector<int32_t>> GEContext::GetMultiBatchShapeIndex(uint32_t graph_id) {
  const auto iter = data_index_and_shape_map_.find(graph_id);
  if (iter != data_index_and_shape_map_.end()) {
    return iter->second;
  }
  return {};
}
}  // namespace ge