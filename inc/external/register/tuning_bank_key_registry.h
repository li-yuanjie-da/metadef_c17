/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#ifndef __INC_REGISTER_TUNING_BANK_KEY_REGISTRY_HEADER__
#define __INC_REGISTER_TUNING_BANK_KEY_REGISTRY_HEADER__
#include <memory>
#include <unordered_map>
#include <nlohmann/json.hpp>
#include <string>
#include "graph/ascend_string.h"
#include "common/ge_common/debug/ge_log.h"
#include "external/register/register_types.h"
#include "exe_graph/runtime/tiling_context.h"

#define REGISTER_OP_BANK_KEY_CONVERT_FUN_V2(op, opfunc)                                                                \
  REGISTER_OP_BANK_KEY_CONVERT_FUN_UNIQ_HELPER_V2(op, (opfunc), __COUNTER__)

#define REGISTER_OP_BANK_KEY_CONVERT_FUN_UNIQ_HELPER_V2(optype, opfunc, counter)                                       \
  REGISTER_OP_BANK_KEY_UNIQ_V2(optype, (opfunc), counter)

#define REGISTER_OP_BANK_KEY_UNIQ_V2(optype, opfunc, counter)                                                          \
  static tuningtiling::OpBankKeyFuncRegistryV2 g_##optype##BankKeyRegistryInterf##counter(#optype, (opfunc))

#define REGISTER_OP_BANK_KEY_PARSE_FUN_V2(op, parse_func, load_func)                                                   \
  REGISTER_OP_BANK_KEY_PARSE_FUN_UNIQ_HELPER_V2(op, (parse_func), (load_func), __COUNTER__)

#define REGISTER_OP_BANK_KEY_PARSE_FUN_UNIQ_HELPER_V2(optype, parse_func, load_func, counter)                          \
  REGISTER_OP_BANK_KEY_PARSE_UNIQ_V2(optype, (parse_func), (load_func), counter)

#define REGISTER_OP_BANK_KEY_PARSE_UNIQ_V2(optype, parse_func, load_func, counter)                                     \
  static tuningtiling::OpBankKeyFuncRegistryV2 g_##optype##BankParseInterf##counter(#optype, (parse_func), (load_func))

#define TUNING_TILING_MAKE_SHARED(exec_expr0, exec_expr1)                                                              \
  do {                                                                                                                 \
    try {                                                                                                              \
      exec_expr0;                                                                                                      \
    } catch (...) {                                                                                                    \
      GELOGW("Make shared failed");                                                                                    \
      exec_expr1;                                                                                                      \
    }                                                                                                                  \
  } while (0)

#define DECLARE_STRUCT_RELATE_WITH_OP_V2(op, bank_key, ...)                                                            \
  NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(bank_key, __VA_ARGS__);                                                           \
  bool ParseFuncV2##op##bank_key(const std::shared_ptr<void> &in_args, size_t len,                                     \
    ge::AscendString &bank_key_json_str) {                                                                             \
    if (sizeof(bank_key) != len || in_args == nullptr) {                                                               \
      return false;                                                                                                    \
    }                                                                                                                  \
    nlohmann::json bank_key_json;                                                                                      \
    bank_key_json = *(std::static_pointer_cast<bank_key>(in_args));                                                    \
    try {                                                                                                              \
      std::string json_dump_str = bank_key_json.dump();                                                                \
      bank_key_json_str = ge::AscendString(json_dump_str.c_str());                                                     \
    } catch (std::exception& e) {                                                                                      \
      return false;                                                                                                    \
    }                                                                                                                  \
    return true;                                                                                                       \
  }                                                                                                                    \
  bool LoadFuncV2##op##bank_key(std::shared_ptr<void> &in_args, size_t &len,                                           \
    const ge::AscendString &bank_key_json_str) {                                                                       \
    len = sizeof(bank_key);                                                                                            \
    TUNING_TILING_MAKE_SHARED(in_args = std::make_shared<bank_key>(), return false);                                   \
    nlohmann::json bank_key_json;                                                                                      \
    try {                                                                                                              \
      bank_key_json = nlohmann::json::parse(bank_key_json_str.GetString());                                            \
      auto op_ky = std::static_pointer_cast<bank_key>(in_args);                                                        \
      *op_ky = bank_key_json.get<bank_key>();                                                                          \
    } catch (std::exception& e) {                                                                                      \
          return false;                                                                                                \
    }                                                                                                                  \
    return true;                                                                                                       \
  }                                                                                                                    \
  REGISTER_OP_BANK_KEY_PARSE_FUN_V2(op, ParseFuncV2##op##bank_key, LoadFuncV2##op##bank_key);


namespace tuningtiling {
using OpBankKeyConvertFunV2 = std::function<bool(const gert::TilingContext *, std::shared_ptr<void> &, size_t &)>;
using OpBankParseFunV2 = std::function<bool(const std::shared_ptr<void> &, size_t, ge::AscendString &)>;
using OpBankLoadFunV2 = std::function<bool(std::shared_ptr<void> &, size_t &, const ge::AscendString &)>;

class FMK_FUNC_HOST_VISIBILITY OpBankKeyFuncInfoV2 {
public:
  explicit OpBankKeyFuncInfoV2(const ge::AscendString &optypeV2);
  OpBankKeyFuncInfoV2() = default;
  ~OpBankKeyFuncInfoV2() = default;
  void SetOpConvertFuncV2(const OpBankKeyConvertFunV2 &convert_funcV2);
  void SetOpParseFuncV2(const OpBankParseFunV2 &parse_funcV2);
  void SetOpLoadFuncV2(const OpBankLoadFunV2 &load_funcV2);
  const OpBankKeyConvertFunV2& GetBankKeyConvertFuncV2() const;
  const OpBankParseFunV2& GetBankKeyParseFuncV2() const;
  const OpBankLoadFunV2& GetBankKeyLoadFuncV2() const;
  const ge::AscendString& GetOpTypeV2() const {
    return optypeV2_;
  }

private:
  ge::AscendString optypeV2_;
  OpBankKeyConvertFunV2 convert_funcV2_;
  OpBankParseFunV2 parse_funcV2_;
  OpBankLoadFunV2 load_funcV2_;
};

class FMK_FUNC_HOST_VISIBILITY OpBankKeyFuncRegistryV2 {
public:
  OpBankKeyFuncRegistryV2(const ge::AscendString &optype, const OpBankKeyConvertFunV2 &convert_funcV2);
  OpBankKeyFuncRegistryV2(const ge::AscendString &optype, const OpBankParseFunV2 &parse_funcV2,
    const OpBankLoadFunV2 &load_funcV2);
  ~OpBankKeyFuncRegistryV2() = default;
  static std::unordered_map<ge::AscendString, OpBankKeyFuncInfoV2> &RegisteredOpFuncInfoV2();
};
}  // namespace tuningtiling
#endif
