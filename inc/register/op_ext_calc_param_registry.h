/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INC_REGISTER_OP_EXTRA_CALC_PARAM_REGISTRY_H_
#define INC_REGISTER_OP_EXTRA_CALC_PARAM_REGISTRY_H_

#include <string>
#include <functional>
#include <vector>
#include "graph/node.h"
#include "external/ge_common/ge_api_types.h"
#include "common/opskernel/ops_kernel_info_types.h"

namespace fe {
using OpExtCalcParamFunc = ge::Status (*)(const ge::Node &node);
class OpExtCalcParamRegistry {
  public:
    OpExtCalcParamRegistry() {};
    ~OpExtCalcParamRegistry() {};
    static OpExtCalcParamRegistry &GetInstance();
    OpExtCalcParamFunc FindRegisterFunc(const std::string &op_type) const;
    void Register(const std::string &op_type, const OpExtCalcParamFunc func);

  private:
    std::unordered_map<std::string, OpExtCalcParamFunc> names_to_register_func_;
  };
class OpExtGenCalcParamRegister {
  public:
    OpExtGenCalcParamRegister(const char *op_type, OpExtCalcParamFunc func) noexcept;
};
} // namespace fe

#ifdef __GNUC__
#define ATTRIBUTE_USED __attribute__((used))
#else
#define ATTRIBUTE_USED
#endif

#define REGISTER_NODE_EXT_CALC_PARAM_COUNTER2(type, func, counter)                  \
  static const fe::OpExtGenCalcParamRegister g_reg_op_ext_gentask_##counter ATTRIBUTE_USED =  \
      fe::OpExtGenCalcParamRegister(type, func)
#define REGISTER_NODE_EXT_CALC_PARAM_COUNTER(type, func, counter)                    \
  REGISTER_NODE_EXT_CALC_PARAM_COUNTER2(type, func, counter)
#define REGISTER_NODE_EXT_CALC_PARAM(type, func)                                \
  REGISTER_NODE_EXT_CALC_PARAM_COUNTER(type, func, __COUNTER__)
#endif // INC_REGISTER_OP_EXTRA_CALC_PARAM_REGISTRY_H_
