/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GE_COMMMON_RUNTIME_DEVICE_TILING_KERNEL_CONTEXT_BUILDER_H_
#define GE_COMMMON_RUNTIME_DEVICE_TILING_KERNEL_CONTEXT_BUILDER_H_

#include "graph/node.h"
#include "exe_graph/runtime/compute_node_info.h"
#include "exe_graph/runtime/kernel_context.h"
#include "exe_graph/lowering/buffer_pool.h"
#include "exe_graph/runtime/tiling_context.h"
#include "exe_graph/runtime/kernel_run_context_builder.h"
#include "register/op_impl_space_registry.h"

namespace gert {
struct AddrRefreshedTensor {
  gert::Tensor *host_addr;
  uint64_t device_addr;
};

struct TiledKernelContextHolder {
  uint64_t dev_op_type_addr_{0UL};
  uint64_t dev_op_name_addr_{0UL};
  KernelContext *host_context_{nullptr};
  uint64_t dev_context_addr_{0UL};
  std::vector<uint64_t> output_addrs_;
  uint8_t *host_compute_node_info_{nullptr};
  size_t compute_node_info_size_{0UL};
};

class DeviceTilingContextBuilder {
 public:
  static size_t CalcTotalTiledSize(const ge::OpDescPtr &op_desc);
  DeviceTilingContextBuilder &CompileInfo(void *compile_info);
  DeviceTilingContextBuilder &Deterministic(int32_t deterministic);
  DeviceTilingContextBuilder &PlatformInfo(void *platform_info);
  DeviceTilingContextBuilder &TilingData(void *tiling_data);
  DeviceTilingContextBuilder &AddrRefreshedInputTensor(const std::map<size_t, AddrRefreshedTensor> &index_to_tensor_);
  DeviceTilingContextBuilder &TiledHolder(uint8_t *host_addr, uint64_t dev_addr, size_t max_mem_size);
  DeviceTilingContextBuilder &Workspace(void *workspace);
  ge::Status Build(const ge::NodePtr &node, TiledKernelContextHolder &holder);

 private:
  ge::graphStatus BuildRtTensor(const ge::GeTensorDesc &tensor_desc, ConstTensorAddressPtr address);
  ge::graphStatus BuildPlacementRtTensor(const ge::GeTensorDesc &tensor_desc, Tensor *rt_tensor) const;
  ge::graphStatus BuildIOTensors(ge::OpDesc *const op_desc);

  ge::Status TiledBuild(const ge::NodePtr &node, TiledKernelContextHolder &holder);

  void *compile_info_{nullptr};
  void *platform_info_{nullptr};
  int32_t deterministic_;
  uint64_t dev_begin_{0UL};
  uint8_t *host_begin_{nullptr};
  size_t max_mem_size_{0UL};
  std::map<size_t, AddrRefreshedTensor> index_to_tensor_;
  std::vector<void *> inputs_;
  std::vector<void *> outputs_{TilingContext::kOutputNum};
  KernelRunContextBuilder base_builder_;
};
}  // namespace gert
#endif  // GE_COMMMON_RUNTIME_DEVICE_TILING_KERNEL_CONTEXT_BUILDER_H_