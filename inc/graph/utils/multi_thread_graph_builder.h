/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __INC_METADEF_MULTI_THREAD_GRAPH_BUILDER_H
#define __INC_METADEF_MULTI_THREAD_GRAPH_BUILDER_H

#include <memory>
#include <vector>
#include <mutex>
#include "external/graph/graph.h"
#include "graph/utils/graph_thread_pool.h"

namespace ge {
class MultiThreadGraphBuilder {
 public:
  explicit MultiThreadGraphBuilder(int32_t thread_num);
  ~MultiThreadGraphBuilder() = default;

  Graph &SetInputs(const std::vector<ge::Operator> &inputs, ge::Graph &graph);

 private:
  static graphStatus GetGraphRelatedOperators(const std::vector<Operator> &inputs,
                                              std::vector<OperatorImplPtr> &related_ops);
  static void GetOutputLinkOps(const OperatorImplPtr &op_impl,
                               std::vector<OperatorImplPtr> &output_op_impls);
  static graphStatus WalkForwardOperators(const std::vector<OperatorImplPtr> &vec_ops,
                                          std::vector<OperatorImplPtr> &related_ops);
  void ResetOpSubgraphBuilder(const OpDescPtr &op_desc, OperatorImplPtr &op_impl);

  int32_t thread_num_;
  std::mutex mutex_;
  std::unique_ptr<GraphThreadPool> pool_;
};
} // namespace ge
#endif // __INC_METADEF_MULTI_THREAD_GRAPH_BUILDER_H
