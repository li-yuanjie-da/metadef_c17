/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INC_GRAPH_UTILS_TYPE_UTILS_INNER_H_
#define INC_GRAPH_UTILS_TYPE_UTILS_INNER_H_

#include <string>
#include "graph/def_types.h"
#include "graph/ge_error_codes.h"
#include "graph/types.h"
#include "register/register_types.h"
#include "external/register/register_fmk_types.h"

namespace ge {
class TypeUtilsInner {
 public:
  static bool IsDataTypeValid(const DataType dt);
  static bool IsFormatValid(const Format format);
  static bool IsDataTypeValid(std::string dt); // for user json input
  static bool IsFormatValid(std::string format); // for user json input
  static bool IsInternalFormat(const Format format);

  static std::string ImplyTypeToSerialString(const domi::ImplyType imply_type);
  static graphStatus SplitFormatFromStr(const std::string &str, std::string &primary_format_str, int32_t &sub_format);
  static Format DomiFormatToFormat(const domi::domiTensorFormat_t domi_format);
  static std::string FmkTypeToSerialString(const domi::FrameworkType fmk_type);
  static bool CheckUint64MulOverflow(const uint64_t a, const uint32_t b);
};
}  // namespace ge
#endif  // INC_GRAPH_UTILS_TYPE_UTILS_INNER_H_