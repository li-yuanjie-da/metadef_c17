/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "register/tuning_bank_key_registry.h"
#include "common/ge_common/debug/ge_log.h"

namespace tuningtiling {
OpBankKeyFuncInfoV2::OpBankKeyFuncInfoV2(const ge::AscendString &optypeV2) : optypeV2_(optypeV2) {}

void OpBankKeyFuncInfoV2::SetOpConvertFuncV2(const OpBankKeyConvertFunV2 &convert_funcV2) {
  convert_funcV2_ = convert_funcV2;
}

void OpBankKeyFuncInfoV2::SetOpParseFuncV2(const OpBankParseFunV2 &parse_funcV2) {
  parse_funcV2_ = parse_funcV2;
}

void OpBankKeyFuncInfoV2::SetOpLoadFuncV2(const OpBankLoadFunV2 &load_funcV2) {
  load_funcV2_ = load_funcV2;
}

const OpBankKeyConvertFunV2& OpBankKeyFuncInfoV2::OpBankKeyFuncInfoV2::GetBankKeyConvertFuncV2() const {
  return convert_funcV2_;
}

const OpBankParseFunV2& OpBankKeyFuncInfoV2::GetBankKeyParseFuncV2() const {
  return parse_funcV2_;
}

const OpBankLoadFunV2& OpBankKeyFuncInfoV2::GetBankKeyLoadFuncV2() const {
  return load_funcV2_;
}

std::unordered_map<ge::AscendString, OpBankKeyFuncInfoV2> &OpBankKeyFuncRegistryV2::RegisteredOpFuncInfoV2() {
  static std::unordered_map<ge::AscendString, OpBankKeyFuncInfoV2> op_func_mapV2;
  return op_func_mapV2;
}

extern "C" void _ZN12tuningtiling21OpBankKeyFuncRegistryC1ERKN2ge12AscendStringERKSt8functionIFbRKSt10shared_ptrIvEmRN15ascend_nlohmann10basic_jsonISt3mapSt6vectorSsblmdSaNSA_14adl_serializerESD_IhSaIhEEEEEERKS5_IFbRS7_RmRKSH_EE() {}

OpBankKeyFuncRegistryV2::OpBankKeyFuncRegistryV2(const ge::AscendString &optype, const OpBankKeyConvertFunV2 &convert_funcV2) {
  auto &op_func_mapV2 = RegisteredOpFuncInfoV2();
  const auto iter = op_func_mapV2.find(optype);
  if (iter == op_func_mapV2.cend()) {
    OpBankKeyFuncInfoV2 op_func_info(optype);
    op_func_info.SetOpConvertFuncV2(convert_funcV2);
    (void)op_func_mapV2.emplace(optype, op_func_info);
  } else {
    iter->second.SetOpConvertFuncV2(convert_funcV2);
  }
}

OpBankKeyFuncRegistryV2::OpBankKeyFuncRegistryV2(const ge::AscendString &optype, const OpBankParseFunV2 &parse_funcV2,
                                                 const OpBankLoadFunV2 &load_funcV2) {
  auto &op_func_map = RegisteredOpFuncInfoV2();
  const auto iter = op_func_map.find(optype);
  if (iter == op_func_map.cend()) {
    OpBankKeyFuncInfoV2 op_func_info(optype);
    op_func_info.SetOpParseFuncV2(parse_funcV2);
    op_func_info.SetOpLoadFuncV2(load_funcV2);
    (void)op_func_map.emplace(optype, op_func_info);
  } else {
    iter->second.SetOpParseFuncV2(parse_funcV2);
    iter->second.SetOpLoadFuncV2(load_funcV2);
  }
}
}  // namespace tuningtiling
