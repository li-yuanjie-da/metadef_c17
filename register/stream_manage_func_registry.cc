/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "register/stream_manage_func_registry.h"

namespace ge {
StreamMngFuncRegistry &StreamMngFuncRegistry::GetInstance() {
  static StreamMngFuncRegistry registry;
  return registry;
}

Status StreamMngFuncRegistry::TryCallStreamMngFunc(const StreamMngFuncType func_type, MngActionType action_type,
                                                   MngResourceHandle handle) {
  StreamMngFunc callback_func = LookUpStreamMngFunc(func_type);
  if (callback_func == nullptr) {
    GELOGW("Stream manage func is not found, FuncType is [%u]", static_cast<uint32_t>(func_type));
    return SUCCESS;
  }
  uint32_t ret = callback_func(action_type, handle);
  GELOGI("Call stream manage func, ret = %u!", ret);
  return SUCCESS;
}

void StreamMngFuncRegistry::Register(const StreamMngFuncType func_type, const StreamMngFunc manage_func) {
  std::lock_guard<std::mutex> lock(mutex_);
  type_to_func_[func_type] = manage_func;
}

StreamMngFunc StreamMngFuncRegistry::LookUpStreamMngFunc(const StreamMngFuncType func_type) {
  std::lock_guard<std::mutex> lock(mutex_);
  const auto iter = type_to_func_.find(func_type);
  if (iter == type_to_func_.end()) {
    return nullptr;
  }
  return iter->second;
}

StreamMngFuncRegister::StreamMngFuncRegister(const StreamMngFuncType func_type, const StreamMngFunc manage_func) {
  StreamMngFuncRegistry::GetInstance().Register(func_type, manage_func);
}
}  // namespace ge