/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "register/op_ext_calc_param_registry.h"
#include "proto/task.pb.h"

namespace fe {
OpExtCalcParamRegistry &OpExtCalcParamRegistry::GetInstance() {
  static OpExtCalcParamRegistry registry;
  return registry;
}

OpExtCalcParamFunc OpExtCalcParamRegistry::FindRegisterFunc(const std::string &op_type) const {
  auto iter = names_to_register_func_.find(op_type);
  if (iter == names_to_register_func_.end()) {
    return nullptr;
  }
  return iter->second;
}

void OpExtCalcParamRegistry::Register(const std::string &op_type, const OpExtCalcParamFunc func) {
  names_to_register_func_[op_type] = func;
}

OpExtGenCalcParamRegister::OpExtGenCalcParamRegister(const char *op_type, OpExtCalcParamFunc func) noexcept {
  OpExtCalcParamRegistry::GetInstance().Register(op_type, func);
}
} // namespace fe
