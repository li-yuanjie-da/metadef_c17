/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "exe_graph/lowering/generate_exe_graph.h"
#include "exe_graph/lowering/lowering_global_data.h"
namespace gert {
namespace bg {
GenerateExeGraph::ExeGraphGenerator GenerateExeGraph::generator_ = {nullptr, nullptr, nullptr};

ValueHolderPtr GenerateExeGraph::MakeSureTensorAtHost(const ge::Node *node, LoweringGlobalData &global_data,
                                                      const ValueHolderPtr &addr, const ValueHolderPtr &size) {
  std::vector<bg::ValueHolderPtr> copy_inputs{global_data.GetStream()};
  copy_inputs.emplace_back(global_data.GetOrCreateAllocator({kOnHost, AllocatorUsage::kAllocNodeWorkspace}));
  copy_inputs.emplace_back(addr);
  copy_inputs.emplace_back(size);
  GE_ASSERT_NOTNULL(node);
  const auto op_desc = node->GetOpDescBarePtr();
  GE_ASSERT_NOTNULL(op_desc);
  return bg::DevMemValueHolder::CreateSingleDataOutput("MakeSureTensorAtHost", copy_inputs, op_desc->GetStreamId());
}

ValueHolderPtr GenerateExeGraph::CalcTensorSizeFromShape(ge::DataType dt, const ValueHolderPtr &shape) {
  auto data_type = ValueHolder::CreateConst(&dt, sizeof(dt));
  return ValueHolder::CreateSingleDataOutput("CalcTensorSizeFromShape", {data_type, shape});
}

ValueHolderPtr GenerateExeGraph::FreeMemoryGuarder(const ValueHolderPtr &resource) {
  return ValueHolder::CreateVoidGuarder("FreeMemory", resource, {});
}
}  // namespace bg
}  // namespace gert